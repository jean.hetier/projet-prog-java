package si;

public class Score {
	private int score;
    private int niveau;
    private int tirs;
    private int monstresTues;

    public Score() {
    	score = 0;
        niveau = 1;
        tirs = 0;
        monstresTues = 0;
    }
    public void incrementerScore() {
        score ++;
    }

    public int getScore() {
        return score;
    }

    public void incrementerNiveau() {
        niveau++;
    }

    public int getNiveau() {
        return niveau;
    }

    public void incrementerTirs() {
        tirs++;
    }

    public void incrementerMonstresTues() {
        monstresTues++;
    }

    public double getPrecision() {
        if (tirs == 0) {
            return 0;
        }
        return (double) monstresTues / tirs * 100;
    }
}
