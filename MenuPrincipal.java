package si;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.Collections;
import java.util.Comparator;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import si.StockageScores;
import java.util.List;
import java.util.Vector;
public class MenuPrincipal extends JPanel {
    private JTextField textFieldPseudo;
    private JButton btnDemarrer;
    private JLabel lblMeilleursScores;
    private JList<StockageScores> listScores;
  

    public MenuPrincipal() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    	//setLayout(new FlowLayout());
        //JPanel centerPanel = new JPanel();
        //centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
        JLabel label = new JLabel("Macrex Unleashed IV : Fighting for the Galactic Working Class");
        add(label, BorderLayout.NORTH);
        btnDemarrer = new JButton("Démarrer la partie");
        add(btnDemarrer, BorderLayout.CENTER);
        btnDemarrer.requestFocusInWindow();
        textFieldPseudo = new JTextField();
        textFieldPseudo.setHorizontalAlignment(JTextField.CENTER);
        textFieldPseudo.setText("Entrez votre pseudo");
        // pour ne pas avoir à effacer "entrez votre pseudo"
        textFieldPseudo.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                if (textFieldPseudo.getText().equals("Entrez votre pseudo")) {
                    textFieldPseudo.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (textFieldPseudo.getText().isEmpty()) {
                    textFieldPseudo.setText("Entrez votre pseudo");
                }
            }
        });

        JPanel pseudoPanel = new JPanel(new FlowLayout());
        pseudoPanel.add(textFieldPseudo);
        add(pseudoPanel);
        

        lblMeilleursScores = new JLabel("Meilleurs scores :");
        
        add(lblMeilleursScores);

        //add(Box.createVerticalGlue()); // Ajouter un espace flexible

        //add(centerPanel, BorderLayout.CENTER);
        listScores = new JList<>();
        JScrollPane scrollPane = new JScrollPane(listScores,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        add(scrollPane);
        add(Box.createVerticalGlue()); // Ajouter un espace flexible
        
        
    }
    public String getPseudo() {
        return textFieldPseudo.getText();
    }
   
    public void afficherMeilleursScores(Vector<StockageScores> scores) {
    	// Trier la liste par ordre d�croissant de score
        Collections.sort(scores, new Comparator<StockageScores>() {
            @Override
            public int compare(StockageScores s1, StockageScores s2) {
                return Double.compare(s2.getScore(), s1.getScore());
            }});
         // Limiter la liste aux 10 premiers éléments
        	Vector<StockageScores> topScores = new Vector<>(scores.subList(0, Math.min(scores.size(), 10)));

            StringBuilder sb = new StringBuilder("<html> TOP 10 :<ul>");
            //for (StockageScores score : topScores) {
            //    sb.append(score.getPseudo()).append("<li>score : ").append(score.getScore()).append(" - precision : ").append(score.getPrecision()).append("</li>");
            //}
            
            sb.append("</ul></html>");
            lblMeilleursScores.setText(sb.toString());
            listScores.setListData(topScores);
        }

        public void setOnDemarrerPartie(ActionListener actionListener) {
            btnDemarrer.addActionListener(actionListener);
        }
    }


