package si;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

//Classe Plateau représentant le plateau de jeu et implémentant ActionListener et KeyListener pour gérer les actions et les événements clavier
public class Plateau extends JComponent implements ActionListener, KeyListener{
	Vaisseau vaisseau; // Vaisseau du joueur
	Armee armee; // Armée d'envahisseurs
	Chateaux chateau;
	private Score score;
	private int niveau;
	private ArrayList<Projectile> projectiles; // Ajouter cette ligne pour gérer les projectiles
	private Image backgroundImage;
	private long niveauChangeTimestamp;
	private ImageIcon gifIcon;
	
	// Constructeur de la classe Plateau
	public Plateau() {
		super(); // Appeler le constructeur de la classe parente (JComponent)
		
		projectiles = new ArrayList<>(); // Initialiser la liste des projectiles
		chateau= new Chateaux();
		score = new Score(); // Initialiser le score à 0
		backgroundImage = new ImageIcon("C:/Users/jeanh/Downloads/fsi.jpg").getImage();
		vaisseau = new Vaisseau(score); // Créer un nouveau vaisseau
		niveau =1 ; // Initialiser le niveau à 0
		armee = new Armee(niveau); // Créer une nouvelle armée d'envahisseurs
		// Définir la taille du composant en fonction des constantes de largeur et hauteur de la fenêtre
		setPreferredSize(new Dimension(Constantes.Largeur_fenetre, Constantes.Hauteur_fenetre));
		gifIcon = new ImageIcon("C:/Users/jeanh/Downloads/dancegif.gif");
		
	}
	
	// Méthode pour dessiner le plateau de jeu et ses éléments
	public void paintComponent(Graphics g) {
		super.paintComponent(g); // Appeler la méthode paintComponent de la classe parente (JComponent)
		// Dessiner l'image de fond
	    g.drawImage(backgroundImage, 0, 0, getWidth(), getHeight(), this);
		Graphics2D g2 = (Graphics2D)g; // Créer un objet Graphics2D à partir de l'objet Graphics
		vaisseau.affiche(g2); // Afficher le vaisseau
		armee.affiche(g2); // Afficher l'armée d'envahisseurs
		chateau.affiche(g2);
		// Afficher les projectiles
	    for (Projectile p : projectiles) {
	        p.affiche(g2);
	    }
	 // Afficher le score
	    g2.setColor(Color.WHITE);
	    g2.setFont(new Font("Arial", Font.PLAIN, 18));
	    g2.drawString("Score: " + score.getScore(), 10, 20);
	 // Afficher le niveau
	    g2.setColor(Color.WHITE);
	    g2.setFont(new Font("Arial", Font.PLAIN, 18));
	    g2.drawString("Niveau: " + niveau, 10, 40); // Afficher le niveau sous le score
	 // Dans la méthode paintComponent de la classe Plateau
	    g2.setColor(Color.WHITE);
	    g2.setFont(new Font("Arial", Font.PLAIN, 18));
	    g2.drawString("Précision : " + String.format("%.1f", score.getPrecision()) + " %", 10, 60);
	    afficherChangementNiveau(g2);
	    g2.drawImage(gifIcon.getImage(), 10, 80, null);
	    

	 

	}
	public void niveauSuivant() {
	    niveau++; // Incrementer le niveau
	    armee = new Armee(niveau); // Créer une nouvelle armée d'envahisseurs
	}
	public int getNiveau() {
        return niveau;
    }
	private void afficherChangementNiveau(Graphics2D g2) {
	    long currentTime = System.currentTimeMillis();
	    long timeSinceLevelChange = currentTime - niveauChangeTimestamp;

	    if (timeSinceLevelChange < 2000) {
	        g2.setColor(Color.RED);
	        g2.setFont(new Font("Arial", Font.BOLD, 48));
	        String levelText = "Niveau " + score.getNiveau();
	        FontMetrics fm = g2.getFontMetrics();
	        int textWidth = fm.stringWidth(levelText);
	        int textHeight = fm.getHeight();
	        g2.drawString(levelText, (Constantes.Largeur_fenetre / 2) - (textWidth / 2), (Constantes.Hauteur_fenetre / 2) - (textHeight / 2));
	    }
	}
	// Méthode pour gérer les actions, comme la mise à jour du déplacement du vaisseau
	@Override
	public void actionPerformed(ActionEvent e) {
	    vaisseau.deplacer();
	    armee.miseAJour(projectiles, getWidth(), score, niveau);
	    // Vérifier si l'armée d'envahisseurs a été détruite
	    if (armee.estDetruite()) {
	        score.incrementerNiveau();
	        niveauChangeTimestamp = System.currentTimeMillis();
	        niveauSuivant(); 
	    }

	    repaint();

	    // Déplacer les projectiles et vérifier les collisions
	    for (int i = 0; i < projectiles.size(); i++) {
	        Projectile p = projectiles.get(i);
	        p.deplacer();

	        // Supprimer les projectiles qui sortent de la fenêtre
	        if (p.getRectangle().y + Constantes.HAUTEUR_TIR_VAISSEAU < 0) {
	            projectiles.remove(i);
	            i--;
	        }
	    }
	
	    repaint();
	}

	// Méthode non utilisée pour gérer les événements de saisie au clavier
	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	// Méthode pour gérer les événements de pression de touche
	@Override
	public void keyPressed(KeyEvent e) {
	    switch (e.getKeyCode()) {
	        case KeyEvent.VK_LEFT:
	            vaisseau.seDeplacerGauche();
	            repaint();
	            break;
	        case KeyEvent.VK_RIGHT:
	            vaisseau.seDeplacerDroite();
	            repaint();
	            break;
	        case KeyEvent.VK_SPACE:
	        	// Ajouter cette section pour la barre d'espace
	        	long tempsDepuisDernierTir = System.currentTimeMillis() - vaisseau.getTempsDernierTir();
	            if (tempsDepuisDernierTir >= 1000) { // Durée de recharge : 1 seconde
	                int x = (int) vaisseau.vaisseau.getCenterX();
	                int y = (int) vaisseau.vaisseau.y;
	                projectiles.add(new Projectile(x, y, Constantes.LARGEUR_TIR_VAISSEAU, Constantes.HAUTEUR_TIR_VAISSEAU, Constantes.DY_TIR_VAISSEAU));
	                vaisseau.tirerProjectile();
	                repaint();
	            }
	            
	            break;
	    }
	}

	// Méthode non utilisée pour gérer les événements de relâchement de touche
	@Override
	public void keyReleased(KeyEvent e) {
		
	}
}

