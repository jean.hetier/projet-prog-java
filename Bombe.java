import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Random;

public class Bombe {
    private Rectangle rect;
    private int vitesse;
    

    public Bombe(int x, int y, int largeur, int hauteur, int vitesse) {
        rect = new Rectangle(x, y, largeur, hauteur);
        this.vitesse = vitesse;
        
    }

    // Méthode pour déplacer la bombe vers le bas
    public void deplacer() {
        rect.y += vitesse/2;
    }

    // Méthode pour obtenir le rectangle de collision de la bombe
    public Rectangle getRectangle() {
        return rect;
    }

    // Méthode pour afficher la bombe
    public void affiche(Graphics2D g2) {
        g2.setColor(Color.BLACK);
        g2.fill(rect);
    }
}
