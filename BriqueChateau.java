import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.Stroke;
import java.awt.BasicStroke;


public class BriqueChateau {
	Rectangle2D.Double brique;
	Color colorfond;
	
	public BriqueChateau(int x, int y) {
		brique = new Rectangle2D.Double(x,y,15,15);
	}
	
	public BriqueChateau(int x, int y, int l, int h, Color c) {
		brique = new Rectangle2D.Double(x,y,l,h);
		colorfond = c;
		}
	
	public void affiche(Graphics2D g2) {
		Color colorfond = this.colorfond;
        g2.setColor(colorfond);
        g2.fill(brique);
        /*
        g2.setColor(Color.green);
        Stroke oldStroke = g2.getStroke(); // sauvegarde de l'ancienne épaisseur de trait
        g2.setStroke(new BasicStroke(2)); // définition de la nouvelle épaisseur de trait
        g2.draw(brique); // dessin du vaisseau
        g2.setStroke(oldStroke); // restauration de l'ancienne épaisseur de trait
        */
    }
		
	public void afficheT(Graphics2D g2) {
		g2.setColor(Color.red);
        g2.fill(brique);
	}
	
	/*
	arrayList
	boucle for i in arrayList
	i.affiche(monstre);
*/
}
