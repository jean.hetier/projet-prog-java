package si;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import java.awt.Stroke;
import java.awt.BasicStroke;
import java.awt.Rectangle;

public class Monstres {
	Ellipse2D.Double monstre;
	private int x, y, vitesse; // position x et y du monstre
    private int largeur, hauteur; // largeur et hauteur du monstre
    private BufferedImage image;
    private ImageIcon monstreIcon;
    private Image monstreImage;

	public Monstres(int x, int y, int vitesse,String imagePath) {
		this.x = x;
        this.y = y;
        this.vitesse = vitesse;
        this.largeur =Constantes.LARGEUR_ALIEN;
        this.hauteur =Constantes.HAUTEUR_ALIEN-20;
		monstre = new Ellipse2D.Double(x,y,Constantes.LARGEUR_ALIEN,Constantes.HAUTEUR_ALIEN-20);
		monstreIcon = new ImageIcon(imagePath);
	    monstreImage = monstreIcon.getImage().getScaledInstance(Constantes.LARGEUR_ALIEN, Constantes.HAUTEUR_ALIEN, Image.SCALE_DEFAULT);
	}
	
	public void affiche(Graphics2D g2) {
		g2.drawImage(monstreImage, x, y, null);
        //g2.setColor(Color.black);
        //g2.fill(monstre);
		//g2.setColor(Color.pink);
		//Stroke oldStroke = g2.getStroke(); // sauvegarde de l'ancienne épaisseur de trait
		//g2.setStroke(new BasicStroke(3)); // définition de la nouvelle épaisseur de trait
		//g2.draw(monstre); // dessin du vaisseau
		//g2.setStroke(oldStroke); // restauration de l'ancienne épaisseur de trait
    }
	// Méthode pour déplacer le monstre horizontalement
    public void deplacerHorizontalement(int deltaX, int niveau) {
        x += deltaX*niveau/4;
        monstre.setFrame(x, y, largeur, hauteur); // Mettre à jour la position de l'objet monstre
    }
    

    // Méthode pour déplacer le monstre verticalement
    public void deplacerVerticalement(int deltaY) {
        y += deltaY;
        monstre.setFrame(x, y, largeur, hauteur); // Mettre à jour la position de l'objet monstre
    }
	
 // Méthode pour obtenir la position x du monstre
    public int getX() {
        return x;
    }

    // Méthode pour obtenir la largeur du monstre
    public int getLargeur() {
        return Constantes.LARGEUR_ALIEN;
    }
    // Méthode pour obtenir le rectangle de collision du monstre
    public Rectangle getRectangle() {
        return new Rectangle(x, y, largeur, hauteur);
    }
}

