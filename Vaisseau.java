package si;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;

public class Vaisseau {
	Ellipse2D.Double vaisseau;
	int sens,sensy;
	private Image vaisseauImage;
	private int tempsDeRecharge;
    private int tempsDeRechargeMax;
    private int curseurX; // position du curseur sous le vaisseau
    private long tempsDernierTir;
    private Score score;
    private int vie;
	
	public Vaisseau(Score score) {
		vaisseau = new Ellipse2D.Double(Constantes.X_POS_INIT_VAISSEAU, Constantes.Y_POS_VAISSEAU,100,25);
		vaisseauImage = new ImageIcon("/run/media/cdelamare/ESD-USB/M1/S8/Java/workspace/SI/src/si/image/véço.png").getImage().getScaledInstance(Constantes.LARGEUR_VAISSEAU, Constantes.HAUTEUR_VAISSEAU, Image.SCALE_DEFAULT);
		sens = 1;
		sensy = 0;
		tempsDeRecharge = 0;
        tempsDeRechargeMax = 30; // temps de recharge de 30 cycles d'horloge (environ 1 seconde)
        curseurX = Constantes.X_POS_INIT_VAISSEAU; // position initiale du curseur
        tempsDernierTir = System.currentTimeMillis() - 1000; // Initialiser tempsDernierTir pour �tre pr�t � tirer
        this.score = score;
        this.vie = 3;
    }
	
	public void diminuerVie() {
        vie--;
    }
	public int getVie() {
        return vie;
    }
	 public Rectangle2D.Double getRectangle() {
	        return new Rectangle2D.Double(vaisseau.x, vaisseau.y, Constantes.LARGEUR_VAISSEAU, Constantes.HAUTEUR_VAISSEAU);
	    }
	public void deplacer() {
        // V�rifier si le vaisseau atteint les bords de la fen�tre et changer la direction
        if ((sens == 1 && vaisseau.x + 100 >= Constantes.Largeur_fenetre) || (sens == -1 && vaisseau.x <= 0)) {
            sens *= -1;
        }
        if ((sensy == -1 && vaisseau.y <= 590) || (sensy == 1 && vaisseau.y + Constantes.HAUTEUR_VAISSEAU >= Constantes.Y_POS_CHATEAU)) {
            sensy = 0;
        }
        
        this.vaisseau.x += (Constantes.Pas * sens);
        this.vaisseau.y += (Constantes.Pas * sensy);
        // Mettre � jour le temps de recharge du tir
        if (tempsDeRecharge < tempsDeRechargeMax) {
        	tempsDeRecharge++;
        }

        // Mettre � jour la position du curseur en fonction du temps de recharge
        curseurX = Constantes.X_POS_INIT_VAISSEAU + (Constantes.LARGEUR_VAISSEAU - 10) * tempsDeRecharge / tempsDeRechargeMax;
    }

    public void seDeplacerDroite() {
       sens=1;
    }
    public void seDeplacerHaut() {
        sensy=-1;
     }
    public void seDeplacerBas() {
        sensy=1;
     }
    public void seDeplacerGauche() {
        sens=-1;
	}
    
    public Projectile tirerProjectile() {
        long tempsActuel = System.currentTimeMillis();
        if (tempsActuel - tempsDernierTir >= 1000) { // Dur�e de recharge : 1 seconde
            tempsDernierTir = tempsActuel;
            // Incrementer le compteur de tirs lorsque le joueur tire un projectile
            score.incrementerTirs();

            int x = (int) vaisseau.getCenterX();
            int y = (int) vaisseau.y;
            return new Projectile(x, y, Constantes.LARGEUR_TIR_VAISSEAU, Constantes.HAUTEUR_TIR_VAISSEAU, Constantes.DY_TIR_VAISSEAU, false);
        }
        return null;
    }

    public long getTempsDernierTir() {
        return tempsDernierTir;
    }

    //public Rectangle2D.Double getRectangle() {
    //    return vaisseau;
    //}
	public void affiche(Graphics2D g2) {
	
		// Dessiner l'image du vaisseau
	    g2.drawImage(vaisseauImage, (int) vaisseau.getX(), (int) vaisseau.getY(), null);
	 
	 // Dessiner le curseur de recharge
	    long tempsDepuisDernierTir = System.currentTimeMillis() - tempsDernierTir;
	    int hauteurCurseur;
	    if (tempsDepuisDernierTir >= 1000) {
	        hauteurCurseur = Constantes.HAUTEUR_VAISSEAU -10;
	    } else {
	        hauteurCurseur = (int) ((Constantes.HAUTEUR_VAISSEAU-10) * tempsDepuisDernierTir / 1000);
	    }
	    int largeurCurseur = 4; // Largeur du curseur
	    g2.setColor(Color.GRAY);

	    // Placer le curseur � gauche du vaisseau et le limiter � la hauteur du vaisseau
	    g2.fillRect((int) vaisseau.getX() - largeurCurseur - 10, (int) vaisseau.getY() -5 + (Constantes.HAUTEUR_VAISSEAU - hauteurCurseur), largeurCurseur, hauteurCurseur);
	}
        
}


	


   