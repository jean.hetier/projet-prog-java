package si;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

//Importer les classes n�cessaires pour cr�er une interface graphique et g�rer les �v�nements clavier
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.*;

//D�finir la classe SpaceInvaders qui impl�mente l'interface KeyListener pour g�rer les �v�nements clavier
public class SpaceInvaders implements KeyListener{
	Jeu jeu;
	private Timer timer; // Timer pour contr�ler la mise � jour du jeu
	private MusiqueFond musique;
    JFrame f;
    CardLayout cl;
    JPanel cards;
    private static final int NIVEAU_MAX =10; // D�finir le niveau maximal
    private GestionnaireScores gestionnaireScores;
    private boolean victoire = false;
    private boolean defaite = false;

	// Constructeur de la classe SpaceInvaders
	public SpaceInvaders() {
		jeu = new Jeu(); // Cr�er un nouveau conteneur de jeu
		gestionnaireScores = new GestionnaireScores();
		FondEcran fond = new FondEcran();
		Thread t = new Thread(fond); // Créer un nouveau thread pour gérer le défilement du fond
		t.start(); // Démarrer le thread
		// Cr�er un nouveau Timer avec un d�lai de 10 ms et un actionListener pour g�rer les mises � jour du plateau
		timer = new Timer(10,jeu.getPlateau());
		timer.addActionListener(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {	           
	            if (verifierVictoire()){
	            	timer.stop(); // Arr�ter le timer
	            	JOptionPane.showMessageDialog(null, "Félicitations ! Vous avez gagné !", "Victoire", JOptionPane.INFORMATION_MESSAGE);
	            	gestionnaireScores.ajouterScore(jeu.getMenuPrincipal().getPseudo(), jeu.getPlateau().score.getScore(), jeu.getPlateau().score.getPrecision());
	            	jeu.getMenuPrincipal().afficherMeilleursScores(gestionnaireScores.getScores());
	                // R�initialiser le jeu
	                reinitialiserJeu();
	            	//jeu = new Jeu();
	                
	            }
	            if (verifierDefaite()){
	            	timer.stop(); // Arr�ter le timer
	            	JOptionPane.showMessageDialog(null, "Perdu !", "Défaite", JOptionPane.INFORMATION_MESSAGE);
	            	gestionnaireScores.ajouterScore(jeu.getMenuPrincipal().getPseudo(), jeu.getPlateau().score.getScore(), jeu.getPlateau().score.getPrecision());
	            	jeu.getMenuPrincipal().afficherMeilleursScores(gestionnaireScores.getScores());
	                // R�initialiser le jeu
	                reinitialiserJeu();
	            	//jeu = new Jeu();
	                
	            }
	        }
	    });
		f = new JFrame();
		f.add(fond);
        //cl = new CardLayout();
        //cards = new JPanel(cl);
        //cards.add(jeu, "jeu");
		f.setLayout(new BorderLayout());
        f.add(jeu, BorderLayout.CENTER);
        
		// Configurer la fen�tre de l'application
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Fermer l'application lorsque la fen�tre est ferm�e
		 // Utiliser un gestionnaire de disposition FlowLayout centr�
		f.addKeyListener(this); // Ajouter un listener de clavier � la fen�tre
		f.addKeyListener(jeu.getPlateau()); // Ajouter un listener de clavier au plateau
		f.pack(); // Ajuster la taille de la fen�tre pour qu'elle corresponde au contenu
		f.setLocationRelativeTo(null); // Centrer la fen�tre � l'�cran
		f.setVisible(true); // Rendre la fen�tre visible
		jeu.getMenuPrincipal().setOnDemarrerPartie(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            //cl.show(cards, "jeu");
	            f.requestFocus(); // Assurez-vous que la fen�tre a le focus pour recevoir les �v�nements clavier
	            timer.start(); // D�marrer le timer       
	            f.repaint();
	        }
	    });
		jeu.getMenuPrincipal().afficherMeilleursScores(gestionnaireScores.getScores());
		// Ajouter une étiquette pour indiquer que le curseur contrôle le volume de la musique
	    JLabel volumeLabel = new JLabel("Volume de la musique :");
	 // Ajouter le curseur de volume et l'étiquette au panneau
	    JPanel controlPanel = new JPanel();
	    controlPanel.add(volumeLabel);
	 // Ajouter le curseur de volume
	    JSlider volumeSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, 5);
	    volumeSlider.setMajorTickSpacing(10);
	    volumeSlider.setMinorTickSpacing(1);
	    volumeSlider.setPaintTicks(true);
	    volumeSlider.setPaintLabels(true);
	    volumeSlider.setValue(5); // Initialisation de la valeur du curseur
	    controlPanel.add(volumeSlider);
	    f.add(controlPanel, BorderLayout.SOUTH);
		musique = new MusiqueFond("/run/media/cdelamare/ESD-USB/M1/S8/Java/workspace/SI/src/si/image/muzik.wav");
		
	   
		
	    musique.setVolume(volumeSlider.getValue()); // Pour initialiser le volume de la musique en fonction de la valeur du curseur
	    // Ajout d'un timer pour ajouter un délai avant de jouer la musique, ce qui permet de la charger au bon volume
	    Timer musicDelayTimer = new Timer(1000, new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            musique.jouerEnBoucle();
	        }
	    });
	    musicDelayTimer.setRepeats(false); // Pour s'assurer que le timer ne se répète pas
	    musicDelayTimer.start(); // Démarrage du timer
		
	    volumeSlider.addChangeListener(new ChangeListener() {
	        @Override
	        public void stateChanged(ChangeEvent e) {
	            JSlider source = (JSlider) e.getSource();
	            
	             int volume = source.getValue();
	             musique.setVolume(volume);
	             
	            }
	        
	    });
	}

	private void reinitialiserJeu() {
	    jeu.getPlateau().reinitialiser(); // Appeler la méthode reinitialiser() du plateau
	    f.requestFocus(); // Assurez-vous que la fenêtre a le focus pour recevoir les événements clavier
	    timer.start(); // Démarrer le timer       
	    f.repaint();
	}
	
	public boolean verifierVictoire() {
	    if (victoire || jeu.getPlateau().getNiveau() > NIVEAU_MAX) {
	        return true;}
	    else{return false;}
	        
	    }
	public boolean verifierDefaite() {
	    if (defaite || jeu.getPlateau().vaisseau.getVie() ==0 ) {
	        return true;}
	    else{return false;}
	        
	    }
	
	
	// M�thode main pour d�marrer l'application
	public static void main(String[] args) {
		new SpaceInvaders(); // Cr�er une nouvelle instance de SpaceInvaders
		
	}

	// M�thode non utilis�e pour l'instant pour g�rer les �v�nements de saisie au clavier
	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	// M�thode pour g�rer les �v�nements de pression de touche
	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
			case KeyEvent.VK_LEFT: // Si la touche gauche est enfonc�e
			case KeyEvent.VK_RIGHT: // Si la touche droite est enfonc�e
				timer.start(); // D�marrer le timer
			break;
			case KeyEvent.VK_BACK_SPACE: // Si la touche retour arri�re est enfonc�e
				timer.stop(); // Arr�ter le timer
			break;
		}
	}

	// M�thode non utilis�e pour l'instant pour g�rer les �v�nements de rel�chement de touche
	@Override
	public void keyReleased(KeyEvent e) {
		
	}
}
