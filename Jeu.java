package si;

import java.awt.*;
import javax.swing.*;

public class Jeu extends JPanel {
    private MenuPrincipal menuPrincipal;
    private Plateau plateau;

    public Jeu() {
        setLayout(new BorderLayout());

        menuPrincipal = new MenuPrincipal();
        plateau = new Plateau();

        add(menuPrincipal, BorderLayout.WEST);
        add(plateau, BorderLayout.CENTER);
    }
    public Plateau getPlateau() {
        return plateau;
    }

    public MenuPrincipal getMenuPrincipal() {
        return menuPrincipal;
    }

}

