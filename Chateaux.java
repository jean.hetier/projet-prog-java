import java.awt.Graphics2D;
import java.util.ArrayList;


public class Chateaux {
	
private ArrayList<BriqueChateau> chateaux;

    
public Chateaux() {
    chateaux = new ArrayList<BriqueChateau>(); 
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            chateaux.add(new BriqueChateau(250 + j * 15, 530 + i * 15));
        }
    }
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            chateaux.add(new BriqueChateau(400 + j * 15, 530 + i * 15));
        }
    }
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            chateaux.add(new BriqueChateau(550 + j * 15, 530 + i * 15));
        }
    }
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            chateaux.add(new BriqueChateau(700 + j * 15, 530 + i * 15));
        }
    }
    }
    
    
    public void affiche(Graphics2D g2) {
        for (BriqueChateau b : chateaux) {
        	b.affiche(g2);
        }
	}
}
