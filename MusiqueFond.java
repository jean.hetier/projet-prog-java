package si;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class MusiqueFond {

    private Clip clip;

    public MusiqueFond(String fichierMusique) {
        try {
            File file = new File(fichierMusique);
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public void jouerEnBoucle() {
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void stop() {
        clip.stop();
    }
}

