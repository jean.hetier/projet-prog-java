import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Projectile {
    private Rectangle rect;
    private int vitesse;
    private Image image;

    public Projectile(int x, int y, int largeur, int hauteur, int vitesse) {
        rect = new Rectangle(x, y, largeur, hauteur);
        this.vitesse = vitesse;

        // Chargement de l'image
        try {
            image = ImageIO.read(new File("C:\\\\Users\\\\Etienne Leclercq\\\\eclipse-workspace\\\\SpaceInvader\\\\src/missile.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Méthode pour déplacer le projectile vers le haut
    public void deplacer() {
        rect.y -= vitesse/2;
    }

    // Méthode pour obtenir le rectangle de collision du projectile
    public Rectangle getRectangle() {
        return rect;
    }

    // Méthode pour afficher le projectile
    public void affiche(Graphics2D g2) {
        // Dessin de l'image
        if (image != null) {
            g2.drawImage(image, rect.x, rect.y, rect.width, rect.height, null);
        }
    }
}
