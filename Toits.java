import java.awt.Graphics2D;
import java.util.ArrayList;

public class Toits {
	private ArrayList<BriqueToit> toits;
	
	public Toits() {
		toits = new ArrayList<BriqueToit>();
		toits.add(new BriqueToit(250,515,60,15));
		toits.add(new BriqueToit(400,515,60,15));
		toits.add(new BriqueToit(550,515,60,15));
		toits.add(new BriqueToit(700,515,60,15));
	}
	
	public void affiche(Graphics2D g2) {
        for (BriqueToit b : toits) {
        	b.affiche(g2);
        }
    }

}
