import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;



public class BriqueToit {
	Rectangle2D.Double toit;
	
	public BriqueToit(int x, int y, int l, int h) {
		toit = new Rectangle2D.Double(x,y,l,h);
	}
	
		
	public void affiche(Graphics2D g2) {
		g2.setColor(Color.red);
        g2.fill(toit);
        g2.setColor(Color.red);
        Stroke oldStroke = g2.getStroke(); 
        g2.setStroke(new BasicStroke(2)); 
        g2.draw(toit); 
        g2.setStroke(oldStroke);
	}
}