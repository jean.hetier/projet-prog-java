package si;
public abstract class Constantes {
	
	/************************************* FENETRE *************************************/	
	// Dimensions de la fenÃªtre
	public static final int Largeur_fenetre = 1000;
	public static final int Hauteur_fenetre = 700;
	//public static final int MARGE_FENETRE = 50;
	
	/************************************* VAISSEAU *************************************/	
	// Dimensions du vaisseau
	public static final int LARGEUR_VAISSEAU = 100;
	public static final int HAUTEUR_VAISSEAU = 50;
	public static final int TEMPS_RECHARGE_TIR = 1000; // Temps en millisecondes
	public static final int LARGEUR_RECHARGE_TIR = 100;
	public static final int HAUTEUR_RECHARGE_TIR = 50;
	// Position initiale du vaisseau
	public final static int X_POS_INIT_VAISSEAU = (Largeur_fenetre - Hauteur_fenetre)/ 2;
	public final static int Y_POS_VAISSEAU = 600;	
	
	// UnitÃ© de dÃ©placement du vaisseau
	public final static int Pas = 2;
	
	// Limite de dÃ©placement du vaisseau
	public final static int LIMITE_GAUCHE_VAISSEAU = 60;
	public final static int LIMITE_DROITE_VAISSEAU = 940;	
	
	/************************************* ALIEN ***************************************/	
	// Dimensions de l'alien
	public static final int LARGEUR_ALIEN = 100;
	public static final int HAUTEUR_ALIEN= 75;	
	
	// ParamÃ¨tres de position des aliens
	public final static int ALT_INIT_ALIEN = 120;
	public final static int X_POS_INIT_ALIEN = 29 ;
	public final static int ECART_LIGNES_ALIEN = 40;
	public final static int ECART_COLONNES_ALIEN = 10;
	
	// UnitÃ© de dÃ©placement de l'alien
	public final static int DX_ALIEN = 2/4;
	public final static int DY_ALIEN = 20/4;
	public final static int VITESSE_ALIEN = 1;	
	
	// Nombre total d'aliens
	public final static int NOMBRE_ALIENS = 50;
	
	/************************************ TIR VAISSEAU **********************************/	
	// Dimensions du tir
	public static final int LARGEUR_TIR_VAISSEAU = 6;
	public static final int HAUTEUR_TIR_VAISSEAU = 20;	
	
	// UnitÃ© de dÃ©placement du tir
	public final static int DY_TIR_VAISSEAU = 10;
	
	/************************************* CHATEAU *************************************/
	// Dimensions de la brique
	public static final int DIMENSION_BRIQUE = 2;
	
	// Dimensions du chÃ¢teau (multiples des dimensions de la brique)
	public static final int LARGEUR_CHATEAU = 72;
	public static final int HAUTEUR_CHATEAU = 54;
		
	// ParamÃ¨tres de position des chÃ¢teaux
	public final static int Y_POS_CHATEAU = 400;
	public final static int X_POS_INIT_CHATEAU = 39;
	public final static int ECART_CHATEAU = 42;
	
	/************************************ TIR ALIEN ************************************/
	// Dimensions du tir
	public static final int LARGEUR_TIR_ALIEN = 5;
	public static final int HAUTEUR_TIR_ALIEN = 15;	
	
	// UnitÃ© de dÃ©placement du tir
	public final static int DY_TIR_ALIEN = 3;

	/************************************* SOUCOUPE *************************************/	
	// Dimensions de la soucoupe
	public static final int LARGEUR_SOUCOUPE = 42;
	public static final int HAUTEUR_SOUCOUPE = 22;

	// Position initiale de la soucoupe
	public final static int X_POS_INIT_SOUCOUPE = Largeur_fenetre;
	public final static int Y_POS_SOUCOUPE = 50;	

	// UnitÃ© de dÃ©placement de la soucoupe
	public final static int DX_SOUCOUPE = 1;
	
	/************************************* POINTS *************************************/	
	// Points attribuÃ©s pour la destruction des aliens
	public static final int VALEUR_ALIEN_HAUT = 50;
	public static final int VALEUR_ALIEN_MILIEU = 40;
	public static final int VALEUR_ALIEN_BAS = 20;
	public static final int VALEUR_SOUCOUPE = 100;
}

